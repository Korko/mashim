export const GRID_SIZE = 50;

export default class TileMap
{
  ctx: CanvasRenderingContext2D;

  constructor (ctx: CanvasRenderingContext2D)
  {
    this.ctx = ctx;
  }

  draw (posX: number, posY: number, width: number, height: number, zoom: number)
  {
    var step = GRID_SIZE / zoom;

    var startX = (Number.MIN_SAFE_INTEGER + posX) % step;
    var endX = startX + (Math.ceil(width / step) + 1) * step;

    var startY = (Number.MIN_SAFE_INTEGER + posY) % step;
    var endY = startY + (Math.ceil(height / step) + 1) * step;

    for (let posY = startY, even = true; posY < height; posY += step - 16, even = !even) {
      for (let posX = startX; posX < width; posX += step) {

        //Math.floor(posX / step), Math.floor(posY / step)
        let img = new Image();
        img.src = require('@/assets/tiles/tail_1/1.png');

        this.ctx.drawImage(img, posX + (even ? step/2 : 0), posY + (Math.random() < .5 ? 5 : 0), step, step);

      }
    }
  }
}