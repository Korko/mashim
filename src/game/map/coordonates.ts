export default class Point
{
	x: number;
	y: number;

	constructor (x: number, y: number)
	{
		this.x = x;
		this.y = y;
	}

	move (x: number, y: number): Vector
	{
		var v = new Vector(new Point(this.x, this.y), new Point(x, y));

		this.x = x;
		this.y = y;

		return v;
	}
}

export class Vector
{
	origin: Point;
	destination: Point;

	constructor (origin: Point, destination: Point)
	{
		this.origin = origin;
		this.destination = destination;
	}

	get x(): number
	{
		return this.destination.x - this.origin.x;
	}

	get y(): number
	{
		return this.destination.y - this.origin.y;
	}
}