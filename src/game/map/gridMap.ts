export const GRID_SIZE = 256;

export default class GridMap
{
  ctx: CanvasRenderingContext2D;

  constructor (ctx: CanvasRenderingContext2D)
  {
    this.ctx = ctx;
  }

  draw (posX: number, posY: number, width: number, height: number, zoom: number)
  {
    this.ctx.lineWidth = 1;

    var step = GRID_SIZE / zoom;

    var startX = (Number.MIN_SAFE_INTEGER + posX) % step;
    var endX = startX + (Math.ceil(width / step) + 1) * step;

    var startY = (Number.MIN_SAFE_INTEGER + posY) % step;
    var endY = startY + (Math.ceil(height / step) + 1) * step;

    for (let start = startX; start < width; start += step) {
      this.ctx.beginPath();
      this.ctx.moveTo(start, startY);
      this.ctx.lineTo(start, endY);
      this.ctx.stroke();
    }

    for (let start = startY; start < height; start += step) {
      this.ctx.beginPath();
      this.ctx.moveTo(startX, start);
      this.ctx.lineTo(endX, start);
      this.ctx.stroke();
    }
  }
}